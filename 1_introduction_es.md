title: Introducción al On-board software de satélites
class: animation-fade
layout: true

.bottom-bar[[> olman.me/slides](https://olman.me/slides) | {{title}}]

---

class: impact

## {{title}}
### Parte I - Introducción
.botoom[Por Olman Quiros Jimenez

[olman.me/slides](https:\\www.olman.me/slides)
]


---
class: center, middle
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licencia Creative Commons Atribución 4.0 Internacional</a>.

<br/>

<img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>

<br/>

Material de uso libre, disponible en [olman.me/slides](https://www.olman.me/posts/slides/intro-to-obsw/)


---
# {{title}}

|   |   |
|---|---|
| **Parte I**    | - **Introducción** |
| Parte II   | - Funciones del On-board Software |
| Parte III  | - Arquitectura del On-board Software |
| Parte IV   | - Comunicaciones |
| Parte V    | - Telemetria & Telecomandos |
| Parte VI   | - Detección, aislamiento y recuperación de fallas  |
| Parte VII  | - Proceso & tecnologías de desarrollo |
| Parte VIII  | - Verificación & Validación |
| Parte IX | - Conceptos Avanzados |

---
class: center, middle

## Premisa

### ¿Como programar un satélite?


#### 1. ¿Que es el On-board Software?

???
- On-board Software (OBSW)
- Flight Software (FSW)
- Avionics Software 
- Satélite Control Software

Que es y como se crea?

El presente manual tiene como objetivo principal brindar una introducción intuitiva y progresiva al tema de On-boards software,  esta dirigido a estudiantes o cualquier profesional que cuenten con algún conocimiento en software, sistemas embebidos, y/o misiones espaciales, pero ningún conocimiento en el desarrollo de software para naves espaciales.

Importante aclarar que el principal enfoque de este documento son los pequeños satélites (por ejemplo los CubeSats). Y no los grandes satelites, como los creados por agencias espaciales nacionales (ejemplo: NASA, ESA).



---
name: agenda

# Parte I - Introducción
## Agenda
1. Historia
1. Conceptos básicos de misiones espaciales
1. Command and Data Handling Subsystem
1. Computadora a bordo
1. Literatura recomendada


---
class: center, middle
# Historia

---
## Antecedentes - Los primeros satélites - 50s

.col-5[
Finales de los 50s.
- No computadora digital ni software.
- Circuitos analógicos simples. 
]

.col-7[
.center[<img src="https://www.microwavejournal.com/legacy_assets/images/9479_Figure1x500.jpg" alt="Drawing" style="width: 14em;"/>]
.center[.small[(A) Sputnik I .cite[Never Beyond Reach], (B) Explorer I .cite[NASA]]]

.center[<img src="https://www.microwavejournal.com/legacy_assets/images/9479_Figure2x500.jpg" alt="Drawing" style="width: 14em;"/>]
.center[.small[(A) SCORE, (B) Courier .cite[Satellite Communications]]]
]

???

Antes de hablar de OBSW, OBC, debemos entender lo que sucedió antes de su creación. 
Mayores milestones para llegar a las OBC y OBSW de hoy en dia. 

https://www.microwavejournal.com/articles/9840-history-of-mobile-satellite-communications
Sputnik I, on 4 October 1957 (see Figure 1). This launch marked the beginning of the use of artificial Earth satellites to extend and enhance the horizon for radio communications, navigation, weather monitoring and remote sensing. That was soon followed on 31 January 1958 by the launch of the US satellite, Explorer I, also shown in the figure. The development of satellite communications and navigation signified the beginning of the space race. The most significant progress in space technology was on 12 April 1961, when Yuri Gagarin, an officer of the former USSR Air Force, lifted off aboard the Vostok I spaceship from Bailout Cosmodrome and made the first historical manned orbital flight in space.
first active communications satellite named SCORE, launched on 18 December 1958 by the US Air Force. The second satellite, Courier, was launched on 4 October 1960

=> No OBC, no SW, 

---
## Antecedentes - Los primeros vuelos tripulados - 60s
Capsulas Mercury y Vostok
- Control desde Tierra.
- Control basado en técnicas analógicas sencillas.


.center[<img src="https://archive.org/download/S64-14286/S64-14286.jpg" alt="Drawing" style="width: 15em;"/>]
.center[.cite[NASA]]



???

Dog (URSS) and chimpance (USA) missions to test weather humans can survive in space. 
Still no computers, entire spacecraft attitude and orbit controll had to be performed from ground.

Human space flights: Mercury and Vostok space ships did not used digital OBC, but relied on very simplistic analog control techniques.


---
## Antecedentes - Misiones a la Luna - 60s

.col-6[
- Misiones a la Luna requerían de sistemas mucho mas avanzados: 
  - "Environmental Control and Life Support System", control de trayectoria, alunizaje, regresar a Tierra, etc. 
- Imposible realizar el control desde Tierra.

### Capsula Gemini
- Por primera vez se usa una computadora digital en el espacio.
]

.col-6[

.center[
<img src="https://ids.si.edu/ids/deliveryService?id=NASM-A19680273000-NASM2019-10008&max_w=900
" alt="Drawing" style="width: 11em;"/>
<br>
.cite[Smithsonian National Air and Space Museum]]

]


---
## Antecedentes - Gemini Digital Computer - 60s
- Desarrollada por IBM.
- Unica computadora abordo. 
- Consumo eléctrico de aprox. 100 W y peso de 27 kg. 

.col-6[
### Procesador:
- Basado en transistores discretos.
- Instrucciones de 39 bits.
- Frecuencia del clock: 7 kHz
- Cycle time: 140 ms
- RAM: 4096 words of 39 bits (aprox 20 kB).
]
.col-6[
.center[<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Gemini_Guidance_Computer_%28NASM%29.JPG/512px-Gemini_Guidance_Computer_%28NASM%29.JPG" alt="Drawing" style="width: 8em;"/>]
.center[.cite[Tamorlan, Source: Wikipedia, CC License]]
]
???
Single computer for the space ship.
By IBM.


---
## Antecedentes - Gemini Digital Computer - 60s

### Software:
El software fue escrito en .alt[ensamblador] debido a las limitantes de rendimiento y 
memoria. 

Se estructuro en módulos:
- Launch
- Rendezvous
- Docking
- Reentry

Cada modulo debía ser cargado a memoria individualmente dependiendo de la fase 
de la misión.


---
## Antecedentes - Apollo Program - 60s 

.col-6[
Lander Module y Command Module (orbiter) cada uno equipados con una computadora 
digital dedicada. 

Desarrollada por MIT Instrument Laboratory. Basada en Gemini Digital Computer.

Procesador:
- RAM: 2048 words de 16 bits.
- ROM: 36 kWords de 16 bits.
- Clock frequency: 1MHz
]

.col-6[
.center[<img src="images/1/apollo_modules.jpg" alt="Drawing" style="width: 20em;"/>]
.center[.cite[NASA]]
]

---
## Antecedentes - Apollo Program - 60s
### Software

Software fue escrito en .alt[ensamblador]. 

- Equipado con un *real-time multitasking sublayer*, algo parecido a un OS. 
- Soportaba hasta un máximo de 8 procesos, en *fixed time slices* 
(porciones de tiempo fijos).

> **Dato curioso**

> Durante el primer alunizaje del Lander Module, los *fixed time slices* y las 
limitantes computacionales causaron *crashes* cíclicos de algunos procesos, 
por lo que el control tuvo que ser transaccionado a manual por Neil Armstrong.

Ya desde en entonces, la .alt[verificación del software] ha sido el principal reto!

???
## Antecedentes - Mariner 10 - 70s

Sonda interplanetaria. 
- Mission: fly-by, estudiar y fotografiar Venus y Mercurio.
- Lanzamiento: 1974

### Método de control: "Digital Command Sequencer"
- Secuencias de commandos almacenadas en formato binario.
- Cada secuencia se accionaba desde tierra por medio de señales.

```txt
Wait 600s, then turn 10 deg around pitch axis.
```

Datos científicos y almacenados en almacenamiento analógico. Es decir, las 
fotografiás se almacenaban en cintas magnéticas y como señal de TV.

---
## Antecedentes - Voyager 1 & 2 - 70s

Sondas interplanetarias. 
- Misión: estudio de los planetas del sistema solar: Jupiter, Saturno, Urano, 
Neptuno.

.col-6[
### Computadora
- Tres computadoras redundantes: encargadas de comunicaciones y determinación 
y control de la orientación.
- Computadora extra para procesamiento de *payload data*, fotografiás. 
- Protección mecánica contra radiación hecha con tántalo y titanio. 
]
.col-6[
### Software
- Programado en ensamblador.
- Posible realizar actualizaciones en órbita, y asi sucedió en multiples ocasiones
(optimizar y arreglar errores). 


> ** Dato curioso **

> Aun parcialmente operativas. Voyager 1 objeto creado por humanos mas alejado del planeta Tierra. 
]
---
## Antecedentes - Space Shuttle - 80s


### Computadora
"Shuttle Data Processing System", construido con 4 computadoras
redundantes, es decir, ejecutando el mismo SW. Sistema de votación en caso de
errores.


.row[
.col-6[
### Procesador
- AP-101, desarrollado por IBM.
- Radiation hardened. 
- 32 bit registers
- RAM: 424 kBytes (version inicial).
]

.col-6[
### Software
Se desarrollo un propio lenguaje de programación de alto nivel para el 
procesador, con el fin de evitar usar ensamblador:

HAL/S -  "High Level Assembler Language/Shuttle"
]
]

---
## Antecedentes - Galileo - 80s

Sonda interplanetaria:
- Mission: exploración de Jupiter y sus lunas.


### Computadora distribuida
- Sistema de Commandos y Datos (triple redundante)
    - Microprocesador RCA 1802, primer micro comercial radiation-hardened.  
- Sistema de Control de Orientación (doble redundante)
    - Microprocesador AMD 2900
- Computadoras de las cámaras. 


### Software
- Escrito en HAL/S



---
## Antecedentes - Era del MIL Standard 80s/90s
### MIL Standard
- Estándares del Departamento de Defensa de EEUU.
- Por lo general asegura niveles de confiabilidad superiores a estándares
comerciales. 


.col-6[
### MIL-STD-1750: 
- Microprocesador de 16 bits para computadoras de vuelo militares.
- Adoptado ampliamente en el sector espacial:
    - Cassisni
    - Mars y Venus Express
    - Roesetta
    - etc.
]

.col-6[
### Software:
- Numerous of compilers available: C, Fortran, Jovial, Ada. 
]

---
## 2000 - Actualidad - RISC

### RISC
- "Reduced Instruction Set Computer"
- Idea: set de instrucciones muy compacto. Menor tiempo por
instrucción. Frequencies del reloj relativamente altas. 
- Al ser chips mas simples, por lo tanto menor cantidad de transistores, menor
sensibilidad a errores inducidos por radiación.
- Varias implementaciones *rad-hardened*

.col-6[
  - RAD6000 por BAE Systems: 
    - Numerosas misiones NASA: Pathfinder, Mars Polar Lander, Spirit y Opportunity Rovers, Spitzer telescope.
]

.col-6[
  - ERC32 por Gaisler Research AB:
    - Numerosas misiones ESA, DLR, CNES: CryoSat 1 y 2, GOCE, SWARM, TerraSAR-X y TanDEM-X.
]


---
## 2000 - Actualidad - Sistemas Operativos a bordo

Con procesadores mas potentes fue posible transicionar de SW *standalone*, a 
arquitecturas mas complejas, haciendo uso de **Real Time Operating Systems (RTOS)** (Sistemas Operativos en Tiempo Real) y OS de propósito general.

### RTOS mas utilizados
- VxWorks
- RTEMS
- FreeRTOS


### OS de propósito general mas utilizados
- Linux

---
## Actualidad - System on chips (SoC)
Chip que integran todos o gran parte de los módulos que componen un computador  .cite[Wikipedia]. 

Rad-hard SoC son muy comunes hoy en dia en satélites:
  - LEON3FT

Suelen integrar componentes especializados:
  - FPGAs
  - SpaceWire interfaces
  - CCSDS boards


---
## Actualidad - COTS  y Miniaturization

Tambien muy comun hoy en dia:

.col-7[
### COTS
- Uso extensivo de componentes *Commercial off-the-shelf* (COTS).
- Es decir, componentes comerciales de bajo costo.
- Reto: vulnerabilidad a radiación.
  - En combinación de componentes *Space grade*.
  - Redundancia.
  - Técnicas de corrección de errores.
]
.col-5[
### Miniaturization
- Tendencia a reducir tamaño y costo de satellites también se extiende a OBCs.
]


.row[.col[.center[.alt[Gracias principalmente a la aparición de los CubeSat!]]]]

???
## Evolución del hardware




<table class="table table-sm text-center fs-6">
  <tbody>
    <tr>
      <td>Digital Sequencers</td>           <td></td>
    </tr>
    <tr><td><i class="bi bi-arrow-down"></td><td></td></tr>
    <tr>
      <td>Transistor-Based OBC</td>         <td>0.007 MIPS</td>
    </tr>
    <tr><td><i class="bi bi-arrow-down"></td><td></td></tr>
    <tr>
      <td>Microprocessors</td>              <td>0.48 MIPS</td>
    </tr>
    <tr><td><i class="bi bi-arrow-down"></td><td></td></tr>
    <tr>
      <td>MIL Standard Processors</td>      <td>3 MIPS</td>
    </tr>
    <tr><td><i class="bi bi-arrow-down"></td><td></td></tr>
    <tr>
      <td>RISC Processors</td>              <td></td>
    </tr>
    <tr><td><i class="bi bi-arrow-down"></td><td></td></tr>
    <tr>
      <td>System On Chip</td>               <td>2,000,000 MIPS</td>
    </tr>
  </tbody>
</table>

---
class: center, middle
# Conceptos básicos de misiones espaciales

---
# Conceptos básicos de misiones espaciales

Existen un sinfín de tipos de misiones espaciales, solo por nombrar algunos: 
- Comunicaciones
- Observación terrestre
- Telescopios astronómicos
- Laboratorios espaciales
- Observación climatológica
- Cápsulas de transporte
- Navegación terrestre
- etc.


Toda misión espacial se puede describir utilizando el .alt[_Space System Model_].

???

Cada misión espacial difiere en un sinfín de aspectos, tamaño, tipo de satélite.
Componentes, instrumentos, funcionamiento del satélite.
Orbita del satélite.
Numero de satélites.
Localización y numero de estaciones en tierra.

Sin embargo toda misión espacial se puede descomponer en Segmento Terrestre y
Segmento Espacial. 


---
# Conceptos básicos de misiones espaciales
## Space System Model



.col-5[
**Definición según ECSS**

Forma de representar un .alt[sistema espacial] al descomponerlo en .alt[elementos sistema] y las actividades que se pueden realizar en relación a dichos sistemas.
]

.col-7[
.center[
<img src="images/1/ground-space-segment.jpg" alt="Drawing" style="width: 75%;"/>
<br>
.cite[C. Cerqueira, W. A. D. Santos, A. Ambrosio]
]]


???
representation of the space system in terms of its decomposition into system elements, the activities that can be performed on these system elements, the reporting data that reflects the state of these system elements and the events that can be raised and handled for the control of these system elements, activities or reporting data
[ECSS]




.small[Cerqueira, C. S., dos Santos, W. A., & Ambrosio, A. M. (2013). Development of an interface to a spacecraft simulator empowered by virtual reality. Journal on Interactive Systems, 3(3), 1. https://doi.org/10.5753/jis.2012.620]




---
## Space System Model

Descomposición funcional. Cada categoría representa un grupo de elementos físicos.

Space System es el nivel mas alto de organización de una misión espacial.


.mid[
~~~
Space System
├─ Space Segment (SS)
│  ├─ Space Segment System
│  │  ├─ Space Segment Subsystem
│  │  │  ├─ Components/parts
├─ Ground Segment (GS)
│  ├─ Ground Segment System
│  │  ├─ Ground Segment Subsystem
│  │  │  ├─ Components/parts
├─ Launch Segment
│  ├─ ...
├─ Support Segment
│  ├─ ...
~~~
]



---
## Space System Model

Ejemplo:

~~~
Space System:                       Apollo Program
├─ Space Segment                    Spacecraft
│  ├─ Space Segment System:         Lander Module, Command Module
│  │  ├─ Space Segment Subsystem:   Data Handling
│  │  │  ├─ Components/parts:       On-board Computer
│ 
├─ Ground Segment
│  ├─ Ground Segment System:        Operations Center
│  │  ├─ Ground Segment Subsystem:  Telecommunications
│  │  │  ├─ Components/parts        Antenna
~~~




---
## Space Segment

~~~
Space System
│
├─ Space Segment:                  [1-n spacecrafts]
│  │  
│  ├─ Space Segment System:        [1 spacecraft] 
│  │  │ 
│  │  ├─ Payload 
|  |  |
│  │  ├─ Satellite Bus/Platform    <- Nos interesa!
~~~


El .alt[bus] o .alt[plataforma] satelital se refiere a la estructura básica
del satélite y sus subsistemas, que albergan y le dan soporte al .alt[Payload].

???
The .alt[bus] or .alt[platform] refers to the basic satellite structure itself
and the subsystems that support the satellite and the Payload.



---
## Space Segment

<table class="table">
  <thead>
    <tr>
      <th scope="col">Payload</th>
      <th scope="col"></th>
      <th scope="col">Bus/Platform</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>La razón de ser de la misión y el satélite.</td>
      <td></td>
      <td>Albergar y suministrar al Payload.</td>
    </tr>
    <tr>
      <td>Ej: radar, cámara, telescopio, equipo de telecomunicaciones.</td>
      <td></td>
      <td>Subsistemas: Estructura, EPS, AOCS, Thermal, CDHS, COMS</td>
    </tr>
    <tr>
      <td>
        <img src="https://cdn.satsearch.com/product-images/satsearch_product-image_2a1int_dragonfly-aerospace_mantis-imager.png" alt="Drawing" style="width: 50%"/>
        <br>
        <span class="cite">Mantis Imager, Dragonfly Aerospace</span>
      </td>
      <td class="align-middle"><i class="bi bi-arrow-left-right"></i></td>
      <td>
        <img src="https://alchetron.com/cdn/satellite-bus-25926bb3-257d-41c6-baf1-a84dcc04764-resize-750.png" alt="Drawing" style="width: 70%;"/>
        <br>
        <span class="cite">SI-150EP/CP Small Satellite Bus, Advanced Solutions</span>
      </td>
    </tr>

    
  </tbody>
</table>





---
## Space Segment
### Subsistemas

.mid[
~~~
...
├─ Space Segment System
│  ├─ Satellite Bus/Platform
|  |  |
│  |  ├─ Physical Structure
|  |  |
│  |  ├─ Electrical Power Subsystem (EPS)
|  |  |
│  |  ├─ Attitude and Orbit Control Subsystem (AOCS)
|  |  |
│  |  ├─ Thermal Control Subsystem
|  |  |
│  |  ├─ Command and Data Handling Subsystem (CDHS)  <- Nos interesa!
|  |  |
│  |  ├─ Communications Subsystem (COMS)
~~~
]
.small[*Los nombres suelen diferir en la literatura.]


---
# Conceptos básicos de misiones espaciales

.alt[Space Segment (SS)] debe estar en .alt[linea de vista] para establecer 
comunicación con .alt[Ground Segment (GS)].

Durante este periodo de la orbita se establece .alt[contactó] o el .alt[link] 
de comunicación.


.center[
<img src="images/1/satellite_link.png" alt="Drawing" style="width: 30%;"/>
]


---
class: center, middle
# Command and Data Handling Subsystem


---
# Command and Data Handling Subsystem



Definición según SMAD:

> .alt[**Electronica**] y .alt[**software**] usado para recibir y distribuir 
.alt[**commandos**], y para almacenar y transmitir datos científicos y 
.alt[**telemetría**]. 

También conocido como:
- Data Handling Subsystem (DHS)
- Telemetry, Tracking, Command and Monitoring (TTC&M) Subsystem 
- Telemetry, Tracking and Command (TT&C) Subsystem
 


???

According SMAD: 

> Electronics and software used to receive and distribute commands and to store and forward payload data and spacecraft telemetry. 


???
[SMAD]


---
# Command and Data Handling Subsystem



Principales tareas:
- Recibe comandos desde tierra a través de COMS.
- Controlar el resto de subsistemas y Payload.
- Recopilar y almacena telemetría de todo el sistema. 
- Recopilar y almacena datos científicos.
- Envía los datos recopilados a tierra a través de COMS.


En resumen: interfaz entre Ground Segment y Space Segment (subsistemas y Payload).  

~~~
        ┌────────────┐  Commands  ┌──────────┐  Commands  ┌───────────┐
        │ Subsystems │◄───────────│          │◄───────────│  Ground   │
        │     &      │            │   CDHS   │            │  Segment  │
        │  Payload   │───────────►│          ├───────────►│           │
        └────────────┘  Telemetry └──────────┘  Telemetry └───────────┘
~~~

???

Receives commands from the ground via the communications system, passes them to appropiate components and payloads, collects telemetry from across the system,  
stores the telemetry, collects and stores scientific data and forwards the 
real-time and stored data to ground via the COMS.

CDHS contains the flight processor where the flight software runs, 
and contains a non volatile memory where the software is stored. 

---
# Command and Data Handling Subsystem


.center[<img src="https://www.researchgate.net/profile/Olman-Quiros-Jimenez/publication/339280425/figure/fig2/AS:860517283422208@1582174716061/Block-Diagram-of-typical-CubeSat-Subsystems-Organization-The-Subsystems-on-the-left-side_W640.jpg" alt="Drawing" style="width:800px;"/>]




---
# Command and Data Handling Subsystem
## Diseño

El diseño del CDHS varia de misión a misión.

Consta de al menos los siguientes componentes esenciales:
- Computadora a bordo (On-board Computer) (OBC)
- Unidades de almacenamiento de datos
- Bus de datos
- .alt[Software de vuelo]

???
# Command and Data Handling Subsystem
## Diseño

Cual OBC, procesador, memoria, interfaces debemos elegir?

Principales factores para el diseño del CDHS:

<table class="table">
  <tr>
    <th>Factor de diseño</th>
    <th>Requerido por</th>
    <th>Impacta a</th>
  </tr>
  <tr>
    <td>Bus de datos</td>
    <td>Payload y Subsistemas</td>
    <td>Hardware y software</td>
  </tr>
  <tr>
    <td>Requerimientos de procesamiento</td>
    <td>Payload, AOCS, algoritmos</td>
    <td>Procesador, SW, EPS</td>
  </tr>
  <tr>
    <td>Memoria de almacenamiento</td>
    <td>Payload</td>
    <td>Hardware</td>
  </tr>
  <tr>
    <td>Precisión temporal</td>
    <td>Payload y AOCS</td>
    <td>Hardware y SW</td>
  </tr>
</table> 

???
[SMAD]

<table class="table">
  <tr>
    <th>Factor de diseño</th>
    <th>Requerido por</th>
    <th>Impacta a</th>
  </tr>
  <tr>
    <td>Instrument Data interfaces</td>
    <td>Payload requirements</td>
    <td>Hardware and software</td>
  </tr>
  <tr>
    <td>Processing requirements</td>
    <td>Ops concepts, algorithms</td>
    <td>Processor, SW, power</td>
  </tr>
  <tr>
    <td>Data Storage volume</td>
    <td>Payloads, ops concepts</td>
    <td>Hardware design</td>
  </tr>
  <tr>
    <td>Timing accuracy </td>
    <td>Payload</td>
    <td>Hardware and SW design</td>
  </tr>
</table> 


---
class: center, middle
# Computadora a bordo



---
# Computadora a bordo

- Componente central y principal del CDHS.
- Sistema embebido.

<div class="row align-items-center">
.col-4[
  <img src="images/1/galileo_obc.png" alt="galileo_obc" style="width: 10em"/>
  <br>
  Galileo IOV OBC 
  <br>
  .cite[Eickhoff 2012]
]

.col-4[
  <img src="images/1/obc_cpu_example.png" alt="obc_cpu_example" style="width: 13em"/>
  <br>
  OBC CPU board example 
  <br>
  .cite[Eickhoff 2012]
]

.col-4[
  <img src="images/1/endurosat_obc.png" alt="endurosat_obc" style="width: 13em"/>
  <br>
  CubeSat OBC example 
  <br>
  .cite[Endurosat]
]
</div>

---
# Computadora a bordo
## Diseño

Al igual que el CDHS, el diseño de la OBC varia de misión a misión.

Principales elementos de la OBC:
- Procesador
- Memoria RAM
- Memorias no volátiles
- Interfaces de comunicación


.row[
Usualmente se emplean de manera .alt[redundante], es decir mas de un elemento del mismo tipo, con exactamente el mismo propósito, en caso de fallas.
]

---
# Computadora a bordo
## Diseño: Procesador

.row[
.col-6[
  **Típicamente procesadores *space-grade*** 
  - Europa: ERC32 y LEON.
  - EEUU: PowerPC 603, PowerPC 750 y arquitecturas R3000.
  - Japón: Hitachi SuperH.
]

.col-6[
  ***New-Space*/Pequeños satélites**:
  - ARM Cortex
  - Atmel AVR 32
  - Microchip PIC24 & PIC32
  - PowerPC
  - TI MSP430
]
]


---
# Computadora a bordo
## Diseño: Memoria RAM


- Memoria de trabajo del OBSW.
- Memoria volátil.
- Almacenamiento de variables, constantes, estructuras de datos, objetos, buffers, instrucciones*. 
- Lectura y escritura.

---
# Computadora a bordo
## Diseño: Memorias no volátil

.col-6[
**Memoria de *código***
- Solo lecture (ROM).
- Almacenamiento del OBSW.
]

.col-6[
**Memoria de *booteo***
- Solo lecture (ROM).
- Almacenamiento del código de inicialización (bootloader).
- Cargar OBSW en Memoria RAM.
]

.col-6[
**Memoria de configuración**
- Lectura y escritura.
- Almacenamiento de variables esenciales (Spacecraft Configuration Vector (SCV)).
]


.col-6[
**Memoria de almacenamiento**
- Lectura y escritura.
- Almacenamiento masivo. 
- Telemetría y datos científicos.
]



---
# Computadora a bordo
## Diseño: Interfaces de comunicación

.col-4[
**Point-2-Point**
- Conexión dedicada entre OBC y subsistema.
- Conexión analógicas con sensores (termómetros, *sun-sensors*).
- Ejemplos: UART/USART, RS232, RS422, LVDS.


]

.col-4[
**Bus de datos**
- Conexión compartida entre varios dispositivos. 
- Cada dispositivo require su controlador.
- Ejemplos: CAN, I2C, SPI, MIL-STD-1553B.
]

.col-4[
**Controladores de red**
- Redes y subredes de dispositivos.
- Require routers/switches y controlador en cada dispositivo.
- Ejemplos: IP, SpaceWire.
]


---
## Diferencias entre OBC y computadoras en tierra

.col-6[
**OBC**
- Sistema embebido, sin interfaz gráfica.
- Costo muy elevado.
- Mecánicamente robustas para soportar altas vibraciones y aceleraciones durante el lanzamiento.
- Soportar condiciones espaciales: vacío, ciclos térmicos y radiación.
- Consumo energético muy limitado.
- Confiabilidad es prioridad.
]

.col-6[
**PC**
- *Plug-an-play*.
- Bajo costo, desechables.
- Consumo energético virtualmente ilimitado. 
- Usualmente mucho mas poderosas computacionalmente. 
- Confiabilidad no son prioridad.
]



???
- Requerimientos *Hard real-time*, determinismo y confiabilidad.
vs
- Precision temporal, determinismo y confiabilidad no son prioridad.
[SMAD] 



---
# Computadora(s) a bordo

.alt[**Aclaracion**]: por lo general la OBC no es la única computadora a bordo de un satélite. 

Hoy en dia, los satélites son inherentemente .alt[sistemas computacionales distribuidos]. 

Otras possibles computadoras en un satélite:
- Computadora de Payload / instrumentos (p.ej.: cámaras, sensores, telescopios, radares)
- Unidades de almacenamiento externo
- Subsistema de Telecomunicaciones 
  - Radio-transmisores
- Subsistema de Potencia (EPS)
- Subsistema de Determinación y Control de la Orientación (ADCS)
  - Y sus sensores y actuadores: Star trackers, GPS/Galileo/Glonass receivers, Giroscopios, Reaction wheels,Propulsores, etc.



---
# Computadora a bordo
## On-board Software

- On-board Software (OBSW)
- Flight Software (FSW)
- Avionics Software 
- Satélite Control Software

### En resumen:
Es el software que se ejecuta en la OBC.

## Que funciones debe realizar? -> Parte II


---
class: center, middle
# Literatura recomendada


---
# Literatura recomendada
- "Onboard Computers, Onboard Software and Satellite Operations" by Eickhoff
- “Handbook of Free and Open Space Standards.” by The LibreCube Initiative
- "State-of-the-Art: Small Spacecraft Technology" by NASA

### Estándares de interés
- Consultative Committee for Space Data Systems (CCSDS)
- European Cooperation for Space Standardization (ECSS)

---
# Referencias
- Eickhoff, Jens. 2012. Onboard Computers, Onboard Software and Satellite Operations. Springer Aerospace Technology. Berlin, Heidelberg: Springer Berlin Heidelberg. https://doi.org/10.1007/978-3-642-25170-2.
- Cubesat Handbook. Elsevier, 2021. doi: 10.1016/C2018-0-02366-X.
- Handbook of Free and Open Space Standards. LibreCube Initiative, Apr. 2021.
- ECSS, Space Engineering: Test and operations procedure
language, Tech. Rep. ECSS-E-ST-70-32C, European Cooperation for Space Standardization, 2008.

---
class: center, middle
# Gracias por su atención!


Preguntas o comentarios: [olmanqj@tutamail.com](mailto:olmanqj@tutamail.com)

Por favor completar encuesta de retroalimentación:

[https://go.uniwue.de/563bc](https://go.uniwue.de/563bc)

.center[![feedback survey](images/feedback-survey.png)]




