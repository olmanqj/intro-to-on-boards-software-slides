title: Introducción al On-board software de satélites
class: animation-fade
layout: true

.bottom-bar[
[> olman.me/slides](https://olman.me/slides) | {{title}}
]


---
class: impact

## {{title}}
### Parte III - Arquitectura del On-board Software
.botoom[
Por Olman Quiros Jimenez

[olman.me/slides](https:\\www.olman.me/slides)
]


---
class: center, middle
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licencia Creative Commons Atribución 4.0 Internacional</a>.

<br/>

<img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>

<br/>

Material de uso libre, disponible en [olman.me/slides](https://www.olman.me/posts/slides/intro-to-obsw/)


---
# {{title}}

|   |   |
|---|---|
| Parte I    | - Introducción |
| Parte II   | - Funciones del On-board Software |
| **Parte III**  | - **Arquitectura del On-board Software** |
| Parte IV   | - Comunicaciones |
| Parte V    | - Telemetria & Telecomandos |
| Parte VI   | - Detección, aislamiento y recuperación de fallas  |
| Parte VII  | - Proceso & tecnologías de desarrollo |
| Parte VIII  | - Verificación & Validación |
| Parte IX | - Conceptos Avanzados |


---
# Agenda
1. Diseño del Software
1. Arquitectura del Software
1. Estilos Arquitectónicos
1. Arquitectura por Capas
1. Arquitectura basada en Servicios
1. Descripción del comportamiento
1. Arquitectura de referencia





---
class: center, middle
# Diseño del Software



---
## En los viejos tiempos...

Se desarrollaba el software sin considerar etapa de diseño. 


.row[
.col-7[

### Ventajas
- Prototipado rápido.
- Se reducen costos en cuanto a planeamiento.

### Desventajas
- Software se vuelve inflexible e inmanejable.
- Realizar cambios o añadir funciones es costoso y lento.
]

.col-4[
<img src="images/3/big-ball-of-mud.png" style="width: 14em;"/>
.center[.cite[Robert Laszczak, threedots.tech]]
]

]

-> Mas tarde se le llamo a esto `Big Ball of Mud.`




---
# Diseño del Software

- Etapa temprana en el .alt[proceso de desarrollo].
- Puente entre .alt[**requerimientos**] e .alt[**implementación**].
- Requerimientos derivados de las funciones esperadas *( recordar Parte II)*.

.center[
**Input:** Requerimientos del software.
**Output:** Arquitectura del software.

<img src="images/3/waterfall_model.png" style="width: 14em;"/>
.center[.cite[Peter Kemp & Paul Smith, Wikipedia]]
]


---
# Diseño del Software

.col-6[
- Principal reto en la etapa de diseño: tamaño y complejidad.
- .alt[Clave]: .alt[Divide y vencerás]. 
  - Descomponer en .alt[elementos].
  - Identificar las .alt[interrelaciones]. 
]
.col-6[
<img src="images/3/divide.jpg" style="width: 14em;"/>
]

---
# Diseño del Software
## Componentes de Software


.col-6[
- Unidad de composición de software de alto nivel. 
  - Provee interfaces definidas.
  - Puede requerir interfaces definidas.
- No contienen detalles de implementación (black-box).
- Ayudan a expresar diseño de alto nivel.
  - Principales Elementos y sus relaciones.
- Implementación puede ser por medio de clases, funciones, archivos, etc.
]

.col-6[
  .center[
<img src="images/3/sw_components1.png" style="width: 10em;"/>

.cite[Wikipedia]]
]


---
# Diseño del Software
## Componentes de Software

.center[
<img src="images/3/sw_components2.png" style="width: 21em;"/>

.cite[uml-diagrams.org]

]




---
# Diseño del Software

Que esta mal en la figura?

.center[
<img src="images/3/high_coupling.png" style="width: 28em;"/>
.center[.cite[Wikipedia]]
]

--

- .alt[**Alto acomplamiento.**]
- .alt[**Baja cohesion.**]


---
# Diseño del Software

Como debería ser: 

.center[
<img src="images/3/low_coupling.png" style="width: 28em;"/>
.center[.cite[Wikipedia]]
]


.col-4[
- .alt[**Bajo acoplamiento.**]
- .alt[**Alta cohesion.**]
]

.col-8[
<br>
.alt[-> Correcta separación de responsabilidades.]
]

---
class: center, middle
# Arquitectura del Software

---
# Arquitectura del Software
## Que es?

> “La .alt4[organización] fundamental de un sistema en términos de sus .alt1[componentes], las .alt2[relaciones] entre sí y con el medio ambiente, y los principios que .alt3[guían] su diseño y evolución." 
> [IEEE standard 1471]

<hr>
> "La .alt4[estructura o estructuras] del sistema, que comprenden .alt1[elementos de software], las propiedades externamente visibles de esos elementos, y las .alt2[relaciones] entre  ellos." 
> [Bass et al.]

<hr>
> La arquitectura de un sistema de software es una metáfora, análoga a la arquitectura de un edificio.
> Funciona como un .alt4[plano] para el sistema en desarrollo.
[Wikipedia]



???
“The fundamental organization of a system embodied in its components, their
relationships to each other, and to the environment, and the principles guiding
its design and evolution.” [IEEE standard 1471]


“The structure or structures of the system, which comprise software elements,
the externally visible properties of those elements, and the relationships
among them.” [Bass el al.]

"The primary goal of architectural modeling should be to come to 
a common vision or understanding with respect to how you
intend to build your system(s). In other words, you will model to
understand."
[Ambler]




???
> Refers to the fundamental .alt[structures of a software] system and the .alt[discipline] of creating such structures and systems.

<hr>
> Each structure comprises .alt[software elements, relations among them], and properties of both elements and relations. The architecture of a software system is a metaphor, analogous to the architecture of a building.

<hr>
> It functions as a .alt[blueprint] for the system and the developing project.




---
# Arquitectura del Software
## Por que es importante?

> "El objetivo principal del modelado arquitectónico debe ser llegar a una .alt[visión o comprensión común] con respecto a cómo se pretende construir su(s) sistema(s). En otras palabras, .alt[modelar para entender]."
>
> [Ambler]


- Compartir información entre personas y equipos.
- Dividir la tarea general en sub-problemas más simples.
- Asignar responsabilidades.
- Ayuda a seleccionar lenguajes de programación adecuados y otras tecnologías de software.
- Planeamiento y perdición de utilización de de recursos.

---
# Arquitectura del Software
## Que **NO** es?

- Detalles para la implementación.
- Requerimientos funcionales o no-funcionales.



???
# Arquitectura del Software
## Resultado

El resultado del proceso de diseño arquitectónico es un .alt[**modelo**] arquitectónico que describe cómo se organiza el sistema como un conjunto de componentes y sus relaciones. .cite[ Software Engineering ]


.center[<img src="images/3/bicycle_parts.jpg" width=auto height="250" />]


---
# Arquitectura del Software
## Como se ve?

Existen diferentes maneras de .alt[modelar] diferentes aspectos del software.
 => .alt[**Vistas Arquitectónicas**]
 - Vista funcional
 - Vista de componentes/módulos
 - Vista de procesos/concurrencia
 - Vista de interacción/comportamiento
 - Vista de despliegue
 - Vista de datos


---
# Arquitectura del Software
### Vista de componentes

.center[
<img src="images/3/component-diagram.jpeg" style="width: 23em;"/>
.center[.cite[Lucidchart exaples]]
]


---
# Arquitectura del Software
### Vista de interacción/comportamiento

.center[
<img src="images/3/sequence-diagram.png" style="width: 17em;"/>
.center[.cite[Lucidchart exaples]]
]

---
# Arquitectura del Software
### Vista de despliegue

.center[
<img src="images/3/deployment-diagram.png" style="width: 20em;"/>
.center[.cite[Conceptdraw exaples]]
]





---
# Arquitectura del Software
### Como se hace?

**Opción 1**: Desde cero
- Analysis, descomposición en elementos, identificación de relaciones.

**Opción 2**: Seguir un .alt[**Estilo Arquitectónico**]
- Arquitectura genérica que nos simplifica el proceso de diseño del software.
- Patrones que nos ayuda a no reinventar la rueda. 
- Consiste en una serie de restricciones y convenciones respecto a los elementos arquitectónicos y sus relaciones.
- .alt[**-> Sistemas complejos suelen encarnar mas de un Estilo Arquitectónico.**]

---
# Arquitectura del Software
## Estilos Arquitectónicos

<table class="table">
  <thead>
    <tr>
      <th scope="col">Categoría</th>
      <th scope="col">Estilo Arquitectónico</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Estructural</td>
      <td>
        - Arquitectura por Capas <br>
        - Arquitectura basada en Componentes <br>
        - Arquitectura Microkernel <br>
        - Arquitectura Pipeline
      </td>
    </tr>
    <tr>
      <td>Despliegue</td>
      <td>
        - Arquitectura por Niveles (N-Tier, 3-Tier)
      </td>
    </tr>
    <tr>
      <td>Interacción</td>
      <td>
        - Arquitectura Orientada a Servicios <br>
        - Arquitectura de Microservicios <br>
        - Arquitectura basada en Eventos 
      </td>
    </tr>
  </tbody>
</table>

???
# Arquitectura del Software
## Partición
- .alt[**Técnica**]: agrupamiento de componentes basado en su rol técnico.
  - Ej: presentación, lógica de negocios, base de datos.

- .alt[**Dominio**]: agrupamiento de componentes basado en dominio de aplicación.
  - Ej: usuarios, clientes, productos.


---
class: center, middle
# Arquitectura por Capas


---
# Arquitectura por Capas

- *Layered Architecture*
- Estilo Arquitectónico *de-facto*.
- Presente prácticamente en todo sistema.
- Idea: Organiza componentes en .alt[capas horizontales].
  - .alt[Partición técnica]: Componentes dentro de cada capa relacionados por su funcionalidad.

- Principio de separación de responsabilidades:
  - Entre mas 'abajo', mas generales son las funciones de la Capa.
  - Entre mas 'arriba', mas especializadas son las funciones de la Capa.

???
- Partición: técnica, funcional.

> Def:
> Agrupamiento de funciones, clases o componentes que comparten alta cohesion dentro de un sistema. 


---
# Arquitectura por Capas





- Partición usual

<table class="table table-sm">
  <thead>
    <tr>
      <th scope="col">Capa</th>
      <th scope="col">Responsabilidad</th>
    </tr>
  </thead>
  <tbody>
    <tr class="table-primary">
      <td>Capa de presentacion: Interfaz Gráfica (UI)</td>
      <td>Responsable de manejar interacción con el usuario.</td>
    </tr>
    <tr class="table-success">
      <td>Capa de Lógica de negocios</td>
      <td>Software que es responsable de satisfacer los requerimientos específicos de la aplicación.</td>
    </tr>
    <tr class="table-danger">
      <td>Capas de Servicio</td>
      <td>    
        Software de propósito general que brinda un servicio especifico.<br>
        Ej: Bases de datos, Servicio de logging, Servicio de red.
    </td>
    </tr>
    <tr class="table-warning">
      <td>Sistema Operativo</td>
      <td>Proveer concurrencia, manejo de memoria, manejo de periféricos, sistema de archivos, etc.</td>
    </tr>
    <tr class="table-active">
      <td>Drivers</td>
      <td>Software especifico para el manejo del hardware. </td>
    </tr>
  </tbody>
</table>


---
# Arquitectura por Capas
### Fachadas

.col-6[
- Por lo general, cada capa es compleja y contiene gran numero de funcionalidades.
- Para reducir acoplamiento, cada capa brinda una .alt[Fachada].
- Interface para utilización de la Capa.
  -  Oculta detalles internos de la implementación.
- Ejemplos de .alt[Fachadas]:
  - API (Application Programming Interface)
  - Clase/Interface con métodos públicos
]
.col-6[
.center[
<img src="images/3/facade.jpg" style="width: 20em;"/>
.center[.cite[Taras Zubyk, Thoughts Aloud]]
]
]




---
# Arquitectura por Capas
### Flujo de información

.col-6[
- Flujo de información de manera sequencial, de capa a capa.
- Por medio de la .alt[Fachada].
- Cadena de dependencias:
  - Capas de arriba no pueden funcionar si capas de abajo.
  - Capas de abajo pueden funcionar sin capas de arriba. 
]

.col-6[
.center[
<img src="images/3/layers_flow.png" style="width: 23em;"/>
.center[.cite[Mohit Malhotra, medium.com]]
]
]


???
# Arquitectura por Capas
### Flujo de información

.center[
<img src="images/3/osi_model_flow.jpg" style="width: 20em;"/>
.center[.cite[Larry Duncan, pinterest.de]]
]


---
# Arquitectura por Capas

.col-6[
**Ventajas**
- Simple de entender e implementar.
- Bajo acoplamiento.
- Aislamiento que permite realizar cambios y portabilidad. 
]

.col-6[
**Desventajas**
- Puede resultar sub-óptima (*Sinkhole Problem*).
- Baja flexibilidad y escalabilidad. 
]


---
# Arquitectura por Capas
### Portabilidad
.row[
- Correcta abstracción permiten remplazar o añadir capas de manera sencilla. 
- Capa superior realiza todas las llamadas a la capa inferior por medio de una .alt[*Abstraction Layer*].
]
.row[
.col-6[
- Pequeñas capas de abstracción entre las capas principales.
.small[
- Hardware Abstraction Layer (HAL).
- Operating System Abstraction Layer (OSAL).
- Message Abstraction Layer (MAL).
- Database Abstraction Layer (DBAL or DAL).
]
]
.col-6[
.center[
<img src="images/3/topology_lagers.webp" style="width: 20em;"/>

.cite[M. Richards & N. Ford, Fundamentals of Software  Architecture]]
]
]
---
class: center, middle
# Arquitectura por Capas en OBSW


---
# Arquitectura por Capas en OBSW

Capas presentes en el OBSW:

<table class="table border">
  <tbody>
    <tr class="table-primary">
      <td>Aplication Layer</td>
      <td>Implementar la mayoría de funciones requeridas por la misión.</td>
    </tr>
    <tr class="table-success">
      <td>Network Layer</td>
      <td>Servicio de comunicaciones.</td>
    </tr>
    <tr class="table-danger">
      <td>Operating System</td>
      <td>Proveer concurrencia, manejo de memoria, manejo de periféricos.</td>
    </tr>
    <tr class="table-warning">
      <td>Board Support Package & Drivers</td>
      <td>Librerías especificas para el manejo del hardware.</td>
    </tr>
  </tbody>
</table>

---
# Arquitectura por Capas en OBSW


Capas presentes en el OBSW:

<table class="table border ">
  <tbody>
    <tr class="table-primary">
      <td class="text-center">Aplication Layer</td>
    </tr>
    <tr class="table-success">
      <td class="text-center">Network Layer</td>
    </tr>
    <tr class="table-active">
      <td class="text-center">Operating System Abstraction Layer (OSAL)</td>
    </tr>
    <tr class="table-danger">
      <td class="text-center">Operating System</td>
    </tr>
    <tr class="table-active">
      <td class="text-center">Hardware Abstraction Layer (HAL)</td>
    </tr>
    <tr class="table-warning">
      <td class="text-center">Board Support Package & Drivers</td>
    </tr>
  </tbody>
</table>


---
# Arquitectura por Capas en OBSW

<table class="table border ">
  <tbody>
    <tr class="table-primary">
      <td class="text-center">Aplication Layer</td>
    </tr>
  </tbody>
</table>

- Dependiente de todas las Capas inferiores.
- Implementa funciones especificas de la misión.
- Alta portabilidad.

~~~ C
// libcsp/examples/csp_server_client.c 
		/* Read packets on connection, timout is 100 mS */
		while ((packet = csp_read(conn, 100)) != NULL) {
			switch (csp_conn_dport(conn)) {
			case MY_SERVER_PORT:
				/* Process packet here */
				csp_print("Packet received on MY_SERVER_PORT: %s\n", (char *) packet->data);
				csp_buffer_free(packet);
				...
~~~

---
# Arquitectura por Capas en OBSW

<table class="table border ">
  <tbody>
    <tr class="table-success">
      <td class="text-center">Network Layer</td>
    </tr>
  </tbody>
</table>


- Dependiente de del Sistema Operativo y Drivers.
- No depende de la Capa de Aplicación.
- Implementa funciones de manejo de datos: Procesamiento de paquetes, ....

~~~ C
// libcsp/src/csp_io.c 
csp_packet_t * csp_read(csp_conn_t * conn, uint32_t timeout) {

	csp_packet_t * packet = NULL;

	if (csp_queue_dequeue(conn->rx_queue, &packet, timeout) != CSP_QUEUE_OK) {
		return NULL;
	}

~~~

---
# Arquitectura por Capas en OBSW

<table class="table border ">
  <tbody>
    <tr class="table-active">
      <td class="text-center">Operating System Abstraction Layer (OSAL)</td>
    </tr>
  </tbody>
</table>

- Parte de *Network Layer*,
- Abstrae toda las llamadas hacia la capa del Sistema Operativo.


~~~ C
// libcsp/src/arch/posix/csp_queue.c 
int csp_queue_dequeue(csp_queue_handle_t handle, void * buf, uint32_t timeout) {
	return pthread_queue_dequeue(handle, buf, timeout);
}
~~~

~~~ C
//  libcsp/contrib/windows/csp_queue.c 
int csp_queue_dequeue(csp_queue_handle_t handle, void * buf, uint32_t timeout) {
	return windows_queue_dequeue(handle, buf, timeout);
}
~~~


---
# Arquitectura por Capas en OBSW

<table class="table border ">
  <tbody>
    <tr class="table-active">
      <td class="text-center">Operating System Abstraction Layer (OSAL)</td>
    </tr>
  </tbody>
</table>


~~~ C
// libcsp/src/arch/posix/pthread_queue.c 
int pthread_queue_dequeue(pthread_queue_t * queue, void * buf, uint32_t timeout) {

	/* Get queue lock */
	pthread_mutex_lock(&(queue->mutex));

	ret = wait_item_available(queue, pts);
	if (ret == PTHREAD_QUEUE_OK) {
		/* Coby object to output buffer */
		memcpy(buf, queue->buffer + (queue->out * queue->item_size), queue->item_size);
		queue->items--;
		queue->out = (queue->out + 1) % queue->size;
	}

	pthread_mutex_unlock(&(queue->mutex));

}
~~~



---
# Arquitectura por Capas en OBSW

<table class="table border ">
  <tbody>
    <tr class="table-active">
      <td class="text-center">Operating System Abstraction Layer (OSAL)</td>
    </tr>
  </tbody>
</table>


~~~ C
//  libcsp/contrib/windows/windows_queue.c 
int windows_queue_dequeue(windows_queue_t * queue, void * buf, int timeout) {

	EnterCriticalSection(&(queue->mutex));
	while (queueEmpty(queue)) {
		int ret = SleepConditionVariableCS(&(queue->cond_empty), &(queue->mutex), timeout);
		if (!ret) {
			LeaveCriticalSection(&(queue->mutex));
			return ret == WAIT_TIMEOUT ? WINDOWS_QUEUE_EMPTY : WINDOWS_QUEUE_ERROR;
		}
	}
	memcpy(buf, (unsigned char *)queue->buffer + (queue->head_idx % queue->size * queue->item_size), queue->item_size);
	queue->items--;
	queue->head_idx = (queue->head_idx + 1) % queue->size;

	LeaveCriticalSection(&(queue->mutex));
	WakeAllConditionVariable(&(queue->cond_full));
	return WINDOWS_QUEUE_OK;
}
~~~


???
# Arquitectura por Capas en OBSW

<table class="table border ">
  <tbody>
    <tr class="table-danger">
      <td class="text-center">Operating System</td>
    </tr>
  </tbody>
</table>



---
# Arquitectura por Capas en OBSW

<table class="table border ">
  <tbody>
    <tr class="table-active">
      <td class="text-center">Hardware Abstraction Layer (HAL)</td>
    </tr>
  </tbody>
</table>

- Parte de la capa del Sistema Operativo.
- Abstrae toda las llamadas hacia la capa del *Board Support Package* y *Drivers*.

~~~ C++
// rodos/api/hal/hal_i2c.h

class HAL_I2C : public GenericIOInterface {

	int32_t init(uint32_t speed = 100000);
	void reset();
	bool isWriteFinished();
	bool isReadFinished();
	int32_t write(const uint8_t addr, const uint8_t *txBuf, uint32_t txBufSize);
	int32_t read(const uint8_t addr, uint8_t *rxBuf, uint32_t rxBufSize);
}
~~~

---
# Arquitectura por Capas en OBSW

<table class="table border ">
  <tbody>
    <tr class="table-active">
      <td class="text-center">Hardware Abstraction Layer (HAL)</td>
    </tr>
  </tbody>
</table>

- Implementación especifica para cada hardware en su respectivo archivo.

~~~ C
// src/bare-metal/raspberrypi3/hal/hal_i2c.cpp
int32_t HAL_I2C::init(uint32_t speed) {
    ...
~~~

~~~ C
// src/bare-metal/stm32f4/hal/hal_i2c.cpp
int32_t HAL_I2C::init(uint32_t speed) {
    ...
~~~

~~~ C
// src/bare-metal/va41620/hal/hal_i2c.cpp
int32_t HAL_I2C::init(uint32_t speed) {
    ...
~~~


???
# Arquitectura por Capas en OBSW

<table class="table border ">
  <tbody>
    <tr class="table-warning">
      <td class="text-center">Board Support Package and Drivers</td>
    </tr>
  </tbody>
</table>


---
# Arquitectura por Capas en OBSW
### SmallSatellite Software Framework

.center[
<img src="images/3/sfsf_layers.png" style="width: 24em;"/>

]



---
# Arquitectura por Capas en OBSW
### Core Flight System, NASA

.center[
<img src="images/3/cfs_layers.png" style="width: 20em;"/>

.cite[cFS, NASA]
]

---
class: center, middle
# Arquitectura Orientada a Servicios


---
# Arquitectura Orientada a Servicios

.col-6[
- Idea: proveer .alt[servicios] por medio de .alt[mensajes] que se transmiten a través de un .alt[protocolo de comunicación].  

- Entidades: 
  - .alt[Cliente/Consumidor] 
  - .alt[Servidor]

- Modelo de interacción: .alt[Transaccional (Request - Reply)].
]

.col-6[
.center[
<img src="images/3/soa.png" style="width: 20em;"/>
.cite[Omar Abdel Wahab, researchgate.net]
]
]
???
- Partición: por Dominio.
  - Cada Servicio atiende un Domino especifico.
---
# Arquitectura Orientada a Servicios




Propiedades de un Servicio:
- Prove una funcionalidad .alt[repetible] y con .alt[resultado definido].
- Es una .alt[caja-negra] para los clientes del servicio.
- Puede estar compuesto de otros .alt[Servicios].
- Los servicios pueden ser consumidos por otros componentes de software o por el usuario final. 


???
- Esta .alt[auto-contenido].

---
# Arquitectura Orientada a Servicios

.col-6[
**Ventajas**
- Permite implementar sistemas distribuidos.
- Alta flexibilidad.
- Servicios aislados que facilita realizar cambios y testing. 
]

.col-6[
**Desventajas**
- Mas complejo que Arquitectura por Capas.
- Require de un protocolo de comunicación.
]

---
class: center, middle
# Arquitectura Orientada a Servicios en OBSW


---
# Arquitectura Orientada a Servicios en OBSW

.center[
<img src="images/3/soa_obsw.png" style="width: 20em;"/>
]

---
# Arquitectura Orientada a Servicios en OBSW

~~~ C
// libcsp/src/csp_service_handler.c 

void command_handler(csp_packet_t * packet) {

	switch (packet->cmd_id) {

		case CMD_DUMMY:
          ...

		case CMD_GET_PARAM:
          ...

		case CMD_SET_PARAM: 
          ...

		case CMD_REBOOT_OBC: 
          ...
~~~


---
# Arquitectura Orientada a Servicios en OBSW

~~~ C
// sfsf/src/sfsf_cmd.c
cmd_exit_status_t command_handler(csp_conn_t *conn, cmd_packet_t * cmd_packet)
{
	// Get command routine handle from command table by the command code
	cmd_handle = get_cmd_table_entry(cmd_packet->cmd_code);
    ...
	return cmd_handle->cmd_routine_p(conn, cmd_packet);
}
~~~

~~~ C
cmd_table_t mission_cmd_table = {
	//Code of command              Pointer to routine
	{.cmd_code = CMD_DUMMY,       .cmd_routine_p = &dummy,          ...},
	{.cmd_code = CMD_GET_PARAM,   .cmd_routine_p = &cmd_get_param,  ...},
	{.cmd_code = CMD_SET_PARAM,   .cmd_routine_p = &cmd_set_param,  ...},
	{.cmd_code = CMD_REBOOT_OBC,  .cmd_routine_p = &cmd_reboot_obc, ...}
};
~~~

---
class: center, middle
# Descripción de comportamiento

---
# Descripción de comportamiento
- Hasta ahora nos enfocamos en la estructura del software.
- También es posible .alt[modelar su comportamiento].
- Idea: modelar los aspectos .alt[dinámicos] de un sistema. Mostrar el comportamiento e interacciones entre los componentes del software o con entidades externas (usuarios u otros sistemas). 

Reflejar:
- Cambios de estado
- Flujo de datos
- Comunicaciones 
- Evolución con el paso del tiempo

---
# Descripción de comportamiento
## Como se ve?

Existen diferentes maneras de modelar el comportamiento del software. 

.alt[Vistas de comportamiento]:
- Diagramas de Actividad
- Diagrama de casos de uso
- Maquinas de Estado
- Diagramas de Timing
- Diagramas de Secuencia
- Diagramas de comunicación


---
# Descripción de comportamiento
## Maquinas de Estado

- Ilustran el comportamiento de un sistema por medio de .alt[Estados], y como puede .alt[transicionar] de estado, de acuerdo con los .alt[activadores (*triggers*)] y las restricciones.

.col-6[
Compuesto por:
- Estado inicial
- Estado final*
- Estados Simples
- Estados Compuestos: Maquina de Estado anidada
- Transiciones
]

.col-[
.center[
<img src="images/3/state-machine1.png" style="width: 15em;"/>

.cite[uml-diagrams.org]
] 
]

???

State Machine diagrams illustrate how an element can move between states, classifying its behavior according to transition triggers and constraining guards.

---
# Descripción de comportamiento
## Secuencia de inicialización

.col-7[
~~~ C
int main(){
  // Initialize hardware
  init_hardware();

  // Init Operating System
  init_os();

  // Init Network layer
  init_network();

  // Set Initial Mode and Run the Modes of Operation
  uint system_mode = LAUNCH_MODE;
  while(true){
    modes_of_operation(system_mode);
  }

}
~~~
]

.col-5[
.center[
<img src="images/3/init_sequence.png" style="width: 8em;"/>
]
]


---
# Descripción de comportamiento
##  Modos de Operación
Recordemos de *Parte II*.

Modos de Operación mas comunes:
.col-6[
- Launch
- Detumble
- Deployment
- Calibration
- Standby/Idle
]

.col-6[
- Link/Downlink
- *Main Mission Modes*
- Safe Mode
- Deorbit 
]

.row[
.alt[-> Cada uno de estos Modos de Operación puede ser un Estado Simple o Estado Compuesto.]
]




---
# Descripción de comportamiento
##  Modos de Operación

.col-6[

.center[
<img src="images/3/state_machine_mops.png" style="width: 15em;"/>
]


]
.col-6[
~~~ C
void modes_of_operation(uint system_mode){
  // Execute current Mode of operation
  switch (system_mode){
    case LAUNCH_MODE:
      launch_routine();
      break;
    case DEPLOYMENT_MODE:
      deploy_routine();
      break;
    case DETUMBLE_MODE:
      detumble_routine();
      break;
    case SAFE_MODE:
      safe_reoutine();
      break;
~~~
]

---
# Descripción de comportamiento
##  Modos de Operación

~~~ C
void launch_routine(){
  /* If 30 mins elapsed since release, transition to DEPLOYMENT_MODE */
  if(...)
    system_mode = DEPLOYMENT_MODE;
}
~~~


~~~ C
void deploy_routine(){
  /* Try at most 5 times to release antennas */
  ...
  if(antenna_released)
    system_mode = DETUMBLE_MODE;
  else
    system_mode = SAFE_MODE;
}
~~~



---
class: center, middle
# Arquitecturas de Referencia


---
# Arquitecturas de Referencia
- *Reference Architecture*.
- Son modelos/diseños que se pueden usar como plantilla.
- Creado para un .alt[**Dominio**] especifico.
- Mas concretos que un .alt[Estilo Arquitectónico]
- Se se pueden ver como una instantiation de .alt[Estilo Arquitectónico].


---
# Arquitecturas de Referencia
### SAVOIR, ESA

.center[
<img src="images/3/SAVOIR.jpg" style="width: 30em;"/>
]



---
# Otros Ejemplos

.small[
**Arquitecturas de Referencia**
- [SmallSatellite Software Framework](https://github.com/olmanqj/sfsf)
- [NASA Core Flight System (cFS)](https://github.com/nasa/cFS)
- [NANOSAT MO FRAMEWORK (NMF)](https://nanosat-mo-framework.github.io/)
- [ESA SAVOIR](https://savoir.estec.esa.int/)
- [CCSDS CAST and Spacecraft Onboard Interface Services](https://public.ccsds.org/publications/SOIS.aspx)

**Abstraction Layers**
- [CubeSat Space Protocol](https://github.com/libcsp/libcsp)
  - OSAL: `include/csp/arch/`
  - Implementations: `libcsp/src/arch/<os>/`
- [RODOS Embedded Operating System](https://gitlab.com/rodos/rodos)
  - HAL: `api/hal/`
  - Implementations: `src/bare-metal/<hardware>/hal/`
]

---
# References

- Software Architecture in Practice, Bass et al., Addison-Wesley, 2003
- Agile Architecture: Strategies for Scaling Agile Development, Scott W. Ambler,  http://www.agilemodeling.com/essays/agileArchitecture.htm
- Software Systems Architecture, Rozanski und Woods, Addison-Wesley, 2005
- CubeSat Space Protocol, https://github.com/libcsp/libcsp
- Rodos (Real time Onboard Dependable Operating System), https://gitlab.com/rodos




---
class: center, middle
# Gracias por su atención!


Preguntas o comentarios: [olmanqj@tutamail.com](mailto:olmanqj@tutamail.com)

Por favor completar encuesta de retroalimentación:

[https://go.uniwue.de/563bc](https://go.uniwue.de/563bc)

.center[![feedback survey](images/feedback-survey.png)]
