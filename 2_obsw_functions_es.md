title: Introducción al On-board software de satélites
class: animation-fade
layout: true

.bottom-bar[
[> olman.me/slides](https://olman.me/slides) | {{title}}
]


---
class: impact

## {{title}}
### Parte II - Funciones del On-board Software
.botoom[
Por Olman Quiros Jimenez

[olman.me/slides](https:\\www.olman.me/slides)
]


---
class: center, middle
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licencia Creative Commons Atribución 4.0 Internacional</a>.

<br/>

<img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>

<br/>

Material de uso libre, disponible en [olman.me/slides](https://www.olman.me/posts/slides/intro-to-obsw/)


---
# {{title}}

|   |   |
|---|---|
| Parte I    | - Introducción |
| **Parte II**   | - **Funciones del On-board Software** |
| Parte III  | - Arquitectura del On-board Software |
| Parte IV   | - Comunicaciones |
| Parte V    | - Telemetría & Telecomandos |
| Parte VI   | - Detección, aislamiento y recuperación de fallas  |
| Parte VII  | - Proceso & tecnologías de desarrollo |
| Parte VIII  | - Verificación & Validación |
| Parte IX | - Conceptos Avanzados |


---
class: center, middle
## Premisa

### ¿Como programar un satélite?


#### 2. ¿Qué funciones debe desempeñar el OBSW?




---
name: agenda

# Parte II - Funciones del On-board Software
## Agenda
1. Categorías de funciones del On-board Software
2. Manejo de datos
3. Control de subsistemas

???
4. Análisis de funciones del OBSW
5. Requerimientos de software


---
# Aclaración

Como vimos anteriormente, existen decenas de tipos diferentes de 
misiones espaciales y de computadoras a bordo. 

Cada misión es única y debe cumplir funciones especificas. 

A continuación veremos, una recopilación de posibles funciones que pueden ser implementadas por el OBSW, pero no todas están presentes en todo OBSW.   



???
No existe una manera única de definir las funciones de una OBC ni de desarrollar
el OBSW.

El resto de las presentaciones nos centraremos en el OBSW de pequeños satélites.


---
# Funciones del On-board Software

Ahora bien, si existe una OBSW en todo satélite es por algo, su papel es claro 
y especifico. 

Implementar las funciones del .alt[*Command & Data Handling] Subsystem*.

Podemos dividir sus funciones en dos grandes ramas: 
- .alt[Manejo de datos.]
- .alt[Control de los subsistemas del satélite.]


---
class: center, middle
# Manejo de datos

---
# Funciones del OBSW: Manejo de datos

Ser el componente que entiende los mensajes que se envían desde tierra y poder interpretarlos, por lo tanto brindar a los operadores de la misión un .alt[medio para controlar el satélite] y cada uno de los subsistemas. 

A su vez, ser el componente que envía información del estado del satélite y cada uno de los subsistemas a tierra, con el fin de que los operadores puedan .alt[conocer el estado de la nave espacial] y recibir .alt[datos del Payload].

<br>

**En resumen: .alt[Interfaz entre el Segmento en Tierra  y el satélite.]**

---
# Funciones del OBSW: Manejo de datos

Subcategorías:
- .alt[Telecomandos]: Ejecutar los comandos provenientes de estaciones terrestres.
- .alt[Telemetría]: Recolectar, almacenar y transmitir información del los subsistemas y el payload.
- .alt[Procesamiento de información] para ser transmitida.


<br>
> En la literatura, a esta categoría tambien se le llama *Telemetry & Telecommands (T&TC)*

???
Por esta razón la OBC es el componente central del Data Handling Subsystem.


---
# Funciones del OBSW: Manejo de datos
## Telecomandos

> Envío de indicaciones a distancia mediante un enlace de transmisión.... 
> Utilizando órdenes enviadas para controlar un sistema. [Wikipedia]

No se transmiten las instrucciones o procedimientos en si, si no una
indicación de cual procedimiento ejecutar. 


Instrucciones/procedimientos ya se encuentran almacenados en el satélite como
parte del OBSW.
- .alt[*Spacecraft On-board Procedures*]:
  - Instrucciones en lenguaje maquina como parte del OBSW.
  - Instrucciones en lenguaje interpretado (scripts). 
      - Ej: PLUTO language de ECSS.


---
# Funciones del OBSW: Manejo de datos
## Telecomandos - Tabla/Vector de Telecomandos

El OBSW contiene una tabla o vector, la cual relaciona un identificador único
con un procedimiento. 


<table  style="font-size:75%" class="table table-bordered table-sm">
  <thead>
    <tr class="table-active">
      <th scope="col">Código</th>
      <th scope="col">Nombre</th>
      <th scope="col">Procedimiento</th>
      <th scope="col">Entradas</th>
      <th scope="col">Salidas esperadas</th>
      <th scope="col">Posibles fallos</th>
    </tr>
  </thead>
  <tbody >
    <tr>
      <th scope="row" class="table-success">0x01</th>
      <td class="table-success">reboot_obc</td>
      <td class="table-warning">1. Reiniciar OBC</td>
      <td class="table-warning"></td>
      <td class="table-warning"></td>
      <td class="table-warning"></td>
    </tr>
    <tr>
      <th scope="row" class="table-success">0x02</th>
      <td class="table-success">deploy_antenna</td>
      <td class="table-warning">
          1. Test antenna no desplegada<br>
          2. Si antenna no desplegada...<br>
          3....
      </td>
      <td class="table-warning"></td>
      <td class="table-warning">Sensor de antenna: antena desplegada</td>
      <td class="table-warning">Antenna no desplegada después de 3 intentos</td>
    </tr>
    <tr>
      <th scope="row" class="table-success">0x03</th>
      <td class="table-success">change_orientation</td>
      <td  class="table-warning">
          1. Calcular orientación actual<br>
          2. Calcular cambio de orientación requerida<br>
          3. Comandar actuadores...
      </td>
      <td class="table-warning">Orientación deseada</td>
      <td class="table-warning">Orientación actual concuerda con orientación deseada.</td>
      <td class="table-warning">Acuadores no responden.<br>
          Datos erróneos de sensores.
      </td>
    </tr>
    <tr>
      <th scope="row" class="table-success">...</th>
      <td class="table-success">...</td>
      <td class="table-warning">...</td>
      <td class="table-warning">...</td>
      <td class="table-warning">...</td>
      <td class="table-warning">...</td>
    </tr>
  </tbody>
</table>

<img src="images/telecommand_table.png" alt="Example telecommand table" style="width: 100%;"/>

---
# Funciones del OBSW: Manejo de datos
## Telecomandos

En concreto - Funciones:
1. Desempaquetar/decodificar telecomandos
2. Obtener procedimiento correspondiente
3. Ejecución:
  - Ejecución inmediata
  - Programar ejecución 
4. Capturar resultado
  - Responder a Tierra con el resultado
  - Almacenar resultado para transmisión posterior


---
# Funciones del OBSW: Manejo de datos
## Telecomandos - Estándar PUS
- Packet Utilization Standard (PUS). .cite[ECSS-E-ST-70-41C]
- Estándar desarrollado por ESA para el manejo de telecomandos.
- Recomienda los telecomandos que debe proveer el OBSW.
- Organiza telecomandos en categorías = .alt[**Servicios**].
- Cada .alt[**Servicios**] ofrece varios .alt[**telecomandos**] (sub-servicios).
- En resumen: enviar un telecomando al satélite consta de:
  - .alt[Service ID] [0 a 127].
  - .alt[Telecommand ID] [0 a 127].
- Usado extensivamente en misiones ESA, DLR, CNES.

---
# Funciones del OBSW: Manejo de datos
## Telecomandos - Estándar PUS

.center[PUS services .cite[ECSS-E-70-41A]
<br>
<img src="images/2/pus_services.jpg" alt="pus_services" style="width: 24em"/>
]


---
# Funciones del OBSW: Manejo de datos
## Telecomandos - Estándar PUS

.center[PUS service 1: Telecommand verification .cite[ECSS-E-70-41A]
<br>
<img src="images/2/pus_service_1.jpg" alt="pus_service_1" style="width: 24em"/>
]


---
# Funciones del OBSW: Manejo de datos
## Telecomandos - Flight Planning

También es posible programar la ejecución de telecomandos.

A esto se le conoce como .alt[*scheduling*] o .alt[*flight planning*].

Criterios para ejecutar Telecomando programados:
.col-4[
- Basado en tiempo:
  - Temporizador
  - Periódicamente
  - Fecha y hora absoluta
]

.col-8[
- Basado en eventos:
  - Localización geográfica
  - Orientación
  - Parámetro interno (p.ej.: voltaje de las baterías, temperatura, etc.)
  - Parámetro externo (p.ej.: incidencia de luz solar, nivel de radiación cósmica, etc.)
]

---
# Funciones del OBSW: Manejo de datos
## Telemetría


> La medición remota de magnitudes físicas y el posterior envío de la 
> información hacia el operador del sistema. El término procede del griego tele, 
> 'distancia' y metron, 'medida'. .cite[Wikipedia]

Obtención de información de interés de manera remota.

También conocido como .alt[Housekeeping data].

En concreto - Funciones:
1. Recolectar telemetría.
1. Almacenar telemetría.
1. Procesar telemetría para posterior transmisión.
1. Transmitir telemetría al Segmento Terrestre.

---
# Funciones del OBSW: Manejo de datos
## Telemetría

Que tipo de información?

.alt[**Parámetro**]: una característica o valor que nos ayude a modelar o describir un 
sistema.

.col-7[
Por ejemplo:
- Voltaje de las baterías.
- Temperatura del procesador.
- Habilitar/Deshabilitar reaction-wheel.
- Angulo de apuntamiento en eje *x* del AOCS.
- Tiempo actual de la OBC. 

Entiéndase también como una variable.
]

.col-5[
<br>

> Cientos/miles de parámetros, inclusive en misiones sencillas.
> **Importante:** distinguir entre parámetros descriptivos, que nos ayude a 
entender el estado del satélite y que no sea redundante.]


---
# Funciones del OBSW: Manejo de datos
## Telemetría
### Recolectar telemetría

Recordemos el .alt[Space System Model] -> Estructura jerárquica.

Cada .alt[fuente] de telemetría recolecta y transmite parámetros al sistema en el siguiente nivel superior, también llamado .alt[agregador] de telemetría. 
A su vez el agregador es fuente de telemetría para el sistema en el siguiente 
nivel. Y asi sucesivamente, hasta llegar al usuario final.

Cada .alt[agregador] puede decidir si utilizar, procesar, filtrar, reenviar,
almacenar los datos.



---
# Funciones del OBSW: Manejo de datos
## Telemetría
.center[Flujo de telemetría de fuente a agregador. .cite[Cappelletti et al. ]

<img src="images/2/telemetry_flow_1.jpg" alt="telemetry_flow" style="width: 15em"/>

<br>

<img src="images/2/telemetry_flow_2.jpg" alt="telemetry_flow" style="width: 18em"/>
]


???

[Cappelletti 200] // TODO


???
# Funciones del OBSW: Manejo de datos
## Telemetría

- .alt[Telemetria en tiempo real]: valor de parámetros justo antes de ser
transmitidos, es decir, telemetría generada durante el link.

- .alt[Telemetria almacenada]: telemetría generada mientras el satélite se 
encuentra fuera de linea vista. Por lo tanto se almacena en 
memoria no volátil para su posterior transmisión durante el siguiente link.

- .alt[Telemetría de alta prioridad]: parámetros que deben ser transmitidos con
alta prioridad. Usualmente son los parámetros que se transmiten tan pronto se 
establece un link. Parámetros esenciales, como conteo de resets de la OBC,
voltaje de las baterías, temperatura de las baterías, etc.


???

Realtime Telemetry:
This is telemetry being generated on board during established ground link and
which is directly transmitted to ground.
Playback Telemetry:
This is housekeeping TM which was generated on board during operations of
the S/C out of sight of the ground station and which was intermediately stored
for the purpose of downlink at next ground contact.
High Priority Telemetry:
This includes all event / action service TM and related TC execution validation
TM which is stored on board at time of ground contact start and which has to
be downlinked to ground with enhanced priority for visibility of events /
actions / recoveries which happened during last flight period and for eventual
manual failure identification and recovery by ground operators.

[Eickhoff]


---
# Funciones del OBSW: Manejo de datos
## Telemetría
### Transmisión de telemetría
- Periódicamente: .alt[beacon].
  - Cada *n* segundos transmisión de colección predefinida de parámetros esenciales.
  - Telemetría de alta prioridad y en tiempo real.
- Bajo demanda (por medio de telecomandos):
  - Telemetría en tiempo real o almacenada.
  - Colección predefinida de parámetros.
      - Diagnosticar subsistema especifico.

???
  - Colección personalizada de parámetros*.  


---
# Funciones del OBSW: Manejo de datos
## Comunicaciones
- Procesamiento de paquetes
- Enrutamiento de paquetes
- Repetición y reordenamiento de paquetes
- Codificación y decodificación
- Serializacion y deserialización
- Corrección de errores


---
# Funciones del OBSW: Manejo de datos
### Paquetes de red/datos

> **Wikipedia:**

> .alt[Paquete de red] o .alt[paquete de datos] es cada uno de los .alt[bloques] en que se divide 
la información para enviar, en la capa de red.
En todo sistema de comunicaciones resulta interesante dividir, la información a
enviar, en .alt[bloques de un tamaño máximo conocido], esto simplifica el control de 
la comunicación.

.center[Paquete UART .cite[circuitbasics.com]
<br>
<img src="images/2/packet.png" alt="packet" style="width: 17em"/>
]


---
# Funciones del OBSW: Manejo de datos
## Comunicaciones

### Procesamiento de paquetes
- Leer paquetes provenientes de las interfaces de comunicación.
- Interpretar los segmentos de los paquetes y tomar acciones en base a esto.  


### Enrutamiento de paquetes
- Paquetes pueden no ser destinados a la OBC.
- Retransmitir paquetes a dispositivos finales o GS. 


---
# Funciones del OBSW: Manejo de datos
## Comunicaciones

### Repetición y reordenamiento de paquetes
- Si ocurre perdida de paquetes.
- Solicitar la retransmisión de paquetes faltantes.
- Reordenar datos recibidos una vez completada la transmisión. 


### Codificación y decodificación
- Convertir la representación de la información para que sea entendida por 
otros dispositivos. 
- Convertir entre Big-endian y Little-endian.
- Convertir entre representación binaria o textual. 


---
# Funciones del OBSW: Manejo de datos
## Comunicaciones

### Serializacion y deserialización
- Convertir estructuras de datos complejas en *stream* de bytes.
- Serialization binaria, CSV, Json, XML, etc.


### Corrección de errores
- Incluir bits redundantes durante la codificación.
- Posible corregir errores (flip de bits) durante la transmisión usando los 
bits redundantes.
- *Error Detection and Correction Codes* (EDAC).

---
# Funciones del OBSW: Manejo de datos
## Procesamiento de información

- Almacenamiento 
  - Administrar unidades de almacenamiento. 
  - Almacenar telemetría y datos del Payload en archivos. 
  - Descarga y recepción de archivos (*File Transfer Protocol FTP*).


- Seguridad
  - Encriptación
  - Autenticación

- Compression de datos


---
class: center, middle
# Control de subsistemas

---
# Funciones del OBSW: Control de subsistemas

Ser el componente que tiene la capacidad de .alt[comandar cada uno de los subsistemas], para que de esta manera el satélite pueda desempeñar su tarea .alt[autónomamente].

Al mismo tiempo, ser el componente que .alt[monitorear periódicamente  el estado] y correcto funcionamiento del resto de los subsistemas, y en el caso dado de una anomalía, llevar acabo .alt[acciones correctivas] para asegurar el exito de la misión. 

<br>

**En resumen: .alt[Prover autonomía al satélite.] **

---
# Funciones del OBSW: Control de subsistemas
## Control de los subsistemas del satélite

Subcategorías:
- Modos de Operación
- Control y monitoreo de subsistemas:
  - Subsistema de control de la orientación y orbita (AOCS)
  - Subsistema de Potencia (EPS)
  - Subsistema Térmico
  - Subsistema de Comunicaciones (COMS)
  - Payload e instrumentos
- Detección, Aislamiento y Recuperación de Fallas (*Fault Detection Isolation and Recovery (FDIR)*)



---
# Funciones del OBSW: Control de subsistemas
## Modos de Operación

Forma de organizar el .alt[desarrollo misión], al dividirla en .alt[etapas]. En cada etapa se busca un .alt[objetivo especifico], al llevar acabo acciones definidas.

.center[.no-margin[
<img src="images/2/ops_1.png" alt="modes_of_operations" style="width: 24em"/>

.cite[Phoenix CubeSat, Arizona State University]
]]

???
También llamado *Concepts of Operations (CONOPS)*.
---
# Funciones del OBSW: Control de subsistemas
## Modos de Operación

.col-6[
En concreto - Funciones:
- Transición entre Modos de Operación
  - De manera autónoma.
      - Debido a un evento o anomalía. 
  - Bajo demanda (por medio de telecomandos).
- Ejecución de acciones predefinidas.
]

.col-6[
<img src="images/2/mode_transitions.jpg" alt="mode_transitions" style="width: 90%"/>
.center[.cite[Phoenix CubeSat, Arizona State University]]
]

---
# Funciones del OBSW: Control de subsistemas
## Modos de Operación

Modos de Operación mas comunes:
.mid[
- Launch
- Detumble
- Deployment
- Calibration
- Standby/Idle
- Link/Downlink
- *Main Mission Modes*
- Safe Mode
- Deorbit 
]
---
# Funciones del OBSW: Control de subsistemas
## Subsistema de control de la orientación y orbita (AOCS)

.alt[**Procesamiento de información de sensores:**] convertir de señales 
análogas/digitales a orientación o posición con respecto a un marco de referencia.


|          |           |  |
|--------------|----|------------|
| Giroscopio      | -> | Velocidad de rotación y orientación |
| Sensor solar    | -> | Posición del sol |
| Sensor terrestre| -> | Posición del horizonte terrestre |
| Magnetómetro    | -> | Dirección hacia el Polo Norte magnético |
| Star Tracker    | -> | Posición con respecto a las estrellas |
| GPS             | -> | Longitud, Latitud altura |



---
# Funciones del OBSW: Control de subsistemas
## Subsistema de control de la orientación y orbita (AOCS)

.alt[**Fusión de datos**]: fusionar información de varios sensores para determinar la orientación, posición y trayectoria unificada del satélite con respecto a un 
marco de referencia. 
- Kinematic Integration
- Kalman Filter
- Ephemerides Propagation
- Orbital Propagation


---
# Funciones del OBSW: Control de subsistemas
## Subsistema de control de la orientación y orbita (AOCS)

.alt[**Control de actuadores:**] accionar actuadores con el fin de controlar la
orientación y orbita. 
- Magnetorquers
- Giroscopio
- Reaction Wheels
- Propulsores

<br>

.alt[**-> Ingeniería de control**]

???
- Controlar la orientación y orbita
- Calcular la posición y ubicación de la nave espacial en su órbita
- Comandar maniobras de navegación y orientación a actuadores (motores, propulsores, etc.)


---
# Funciones del OBSW: Control de subsistemas
## Subsistema de Potencia (EPS)

- Controlar el consumo de poder.
- Controlar carga de las baterías.
- Controlar orientación de los paneles solares.



---
# Funciones del OBSW: Control de subsistemas
## Subsistema Térmico
- Controlar la temperatura interna de los componentes esenciales del satélite.
- Activar/desactivar calentadores. 
- Activar/desactivar calentadores radiadores.


---
# Funciones del OBSW: Control de subsistemas
## Subsistema de Comunicaciones (COMS)

- Desplegar antenas una vez en órbita.
- Controlar apuntamiento de las antenas.
- Activar/desactivar radiotransmisores.


---
# Funciones del OBSW: Control de subsistemas
## Payload e Instrumentos científicos
- Desplegar Payload e instrumentos científicos una vez en orbita.
- Calibrar instrumentos y sensores.
- Controlar apuntamiento de instrumentos y sensores.
- Recolección de datos.

<br>

Funciones muy dependiente de la misión.


---
# Funciones del OBSW: Control de subsistemas
## Detección, Aislamiento y Recuperación de Fallas (FDIR)

.col-6[
- Monitorear el estado del sistema.
  - Por medio de telemetría.
- Detectar fallas.
  - Checkeo del valor de parámetros dentro de rangos nominales.
      - Voltaje, amperage, temperatura, etc.
  - *Heartbeats*
  - *Build-in tests*
]
.col-6[
- Llevar a cabo acciones correctivas en caso de fallas.
  - Transicionar a *Safe-Mode*.
  - Transicionar a equipo redundante.
  - Reiniciar subsistemas.
  - Reconfigurar subsistemas.
  - Desactivar subsistemas. 
]


---
# Funciones del OBSW: Control de subsistemas
## Detección, Aislamiento y Recuperación de Fallas (FDIR)

Ejemplos:
- El voltaje de las baterías bajo de 7.0V:
  - Acción correctiva: transición a *Safe Mode* (desactivar todo subsistema no esencial), esperar a que se carguen las baterías por encima de 8.0V.

- Ausencia de *Heartbeat* de subsistema:
  - Acción correctiva: Reiniciar subsistema.


- Temperatura de OBC mucho mas elevada que temperatura nominal:
  - Acción correctiva: transición a *Safe Mode*, desactivar todas las tareas y servicios no esenciales, hasta que se restablezca temperatura nominal.

---
# Funciones del OBSW: Control de subsistemas

## Misceláneo


- Mantener y distribuir el tiempo (fecha/hora).
- Sincronizar reloj interno con el Segmento Terrestre.

???

Lo anterior son solo ejemplos de algunas de las tareas que podría desempeñar la OBC, sin embargo no todas están presentes en toda misión espacial, por ejemplo un satélite sin control activo de la orientación, no tendrá que ejecutar maniobras de navegación y orientación. Por otro lado, hay tareas que si están presentes en todo satélite, por ejemplo ejecutar comandos provenientes de estaciones terrestres, o, preparar información para ser enviada a estaciones terrestres.



---
class: center, middle
# Reto/Tarea

---
# Reto/Tarea
### Simular software de satélite y Ground Station en nuestra computadora.

**Parte 1.**
1. Descargar .alt[Cubesat Space Protocol]: [https://github.com/libcsp/libcsp](https://github.com/libcsp/libcsp)
1. Comprender el funcionamiento del .alt[ejemplo Cliente - Servidor]: [examples/csp_server_client.c](https://github.com/libcsp/libcsp/blob/develop/examples/csp_server_client.c)
1. Compilar y ejecutar el ejemplo.
  - Tip: usar (y comprender) el script [examples/buildall.py](https://github.com/libcsp/libcsp/blob/develop/examples/buildall.py).


---
# Reto/Tarea
### Simular software de satélite y Ground Station en nuestra computadora.


**Parte 2.**
1. Extender el .alt[Servidor] para que envié un .alt[Beacon] (paquete de telemetría) cada 10 segundos.
  - Ser creativos con la telemetría.
1. Extender el .alt[Cliente] para que envié el comando "*1*" cada 15 segundos.
1. Implementar .alt[Vector de Telecomandos] en .alt[Servidor]:
  - Al recibir el Telecomando "*1*": responder con un paquete diferente de telemetría. 
  - Opción 1: Usar 'switch-case'.
  - Opción 2: Usar arreglo de punteros a funciones.



---
# Referencias
- Eickhoff, Jens. 2012. Onboard Computers, Onboard Software and Satellite Operations. Springer Aerospace Technology. Berlin, Heidelberg: Springer Berlin Heidelberg. https://doi.org/10.1007/978-3-642-25170-2.
- Cubesat Handbook. Elsevier, 2021. doi: 10.1016/C2018-0-02366-X.


---
class: center, middle
# Gracias por su atención!


Preguntas o comentarios: [olmanqj@tutamail.com](mailto:olmanqj@tutamail.com)

Por favor completar encuesta de retroalimentación:

[https://go.uniwue.de/563bc](https://go.uniwue.de/563bc)

.center[![feedback survey](images/feedback-survey.png)]
